import React, { Component } from 'react';
import { Button } from 'reactstrap';
import AuthTemplate from './AuthTemplate';
import { Field, Form } from 'react-final-form';
import renderInput from '../components/form/Input';
import { sleep } from '../lib/utils';
import { composeValidators, required, email } from '../lib/validation';
import { FormFooter, StyledLink, Title } from '../components/styled/form';

class SignIn extends Component {
  onSubmit = async data => {
    await sleep(1000);
    console.log(data);
  };

  render() {
    return (
      <AuthTemplate>
        <Title>Sign in your account</Title>
        <Form
          onSubmit={this.onSubmit}
          render={({ handleSubmit, pristine, invalid }) => (
            <form onSubmit={handleSubmit}>
              <Field
                name="email"
                component={renderInput}
                type="text"
                label="Email address:"
                validate={composeValidators(required, email)}
              />
              <Field
                name="password"
                component={renderInput}
                type="password"
                label="Password:"
                validate={required}
              />
              <FormFooter>
                <Button size="lg" color="primary" block>
                  Sign In
                </Button>
                <StyledLink to={'/signup'}>
                  Don't have an account? Create a new one
                </StyledLink>
                <StyledLink to={'/resetpassword'}>Forgot password?</StyledLink>
              </FormFooter>
            </form>
          )}
        />
      </AuthTemplate>
    );
  }
}

export default SignIn;
