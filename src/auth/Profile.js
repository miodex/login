import React, { Component } from 'react';
import styled from 'styled-components';
import {Col, Card, CardImg, FormGroup, Label, Input, FormText, Row, Container, Media,  Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavDropdown,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    Dropdown,
    DropdownItem } from 'reactstrap';
import logo from '../logo.png';
import { Button, Table } from "reactstrap";
import { Form, Field } from "react-final-form";
import AuthTemplate from "./AuthTemplate";
import renderInput from "../components/form/Input";
import renderCheckbox from "../components/form/Checkbox";
import { sleep } from "../lib/utils";
import { required, email, composeValidators } from "../lib/validation";
import { Link } from "react-router-dom";
import colors from "../colors";
import routes from "../routes";
import Avatar from 'react-avatar';
import UserAvatar from 'react-user-avatar';



class SignIn extends Component {
  onSubmit = async data => {
    await sleep(2000);
    console.log(data);
  };
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      
        <div>
           <Navbar1 color="white" light expand="md">
            
            <NavbarBrand href="/">
            <img src={logo}  alt="logo" />
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="mr-auto" navbar>
  
                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
    <Button1 color="whiter"><i class="fas fa-search"/> PAGE TEST</Button1>
    <Button2 color="whiter"><i class="fas fa-cog"/> SETTINGS</Button2>
  
              </Nav>
              <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                 
                  <DropdownItem>
                  edyta@pqstudio.pl
                   </DropdownItem>
                  
                  <DropdownMenu right>
                    <NavItem>
                    <NavLink href="/profile">Settings</NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink href="/">Log Out</NavLink>
                    </NavItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
  </Nav>
  </Collapse>
  </Navbar1>
        <Form
                  onSubmit={this.onSubmit}
                  render={({ handleSubmit, pristine, invalid }) => (
                    <form onSubmit={handleSubmit}>
        
     
        <Table align="center">
      <Prof>
     Profile
     </Prof>
                  <App4>
             <Row>
               <Col sm="2">
              <UserAvatar size="98" name="Will Binns-Smith" src="https://cdn2.iconfinder.com/data/icons/rcons-user/32/female-shadow-fill-circle-512.png" />         
              </Col>

            <Col sm="5">     
        <Field
                        name="name"
                        component={renderInput}
                        type="text"
                        label="Name:"
                        validate={required}
                      />
        
                      <Field 
                       name="last_name"
                       component={renderInput}
                       type="text"
                       label="Last name"
                       validate={required}
                       />       
        <Field 
                       name="email"
                       component={renderInput}
                       type="text" align ="right"
                       label="Adress email:"
                       validate={composeValidators(required, email)}
                      />
        </Col>

       <Col sm="5"> 
        <Field
                        name="password"
                        component={renderInput}
                        type="password"
                        label="Password:"
                        validate={required}
                      />
                       <Button color="primary">  
                      <div class="napislogo2">  <div align="right"><StyledLink to={"/set-password/:token"}>Change password</StyledLink></div>
                      </div>
                       </Button>
        </Col>
      </Row>
   </App4>
  <SaveButton>
   <Button color="secondary">Save</Button>  
   </SaveButton>
   </Table>
        </form>
    
                  )}
                  /> 
                  </div>
    );
  }
}
const StyledLink = styled(Link).attrs({  })`
margin-left: 20px;
margin-right: 20px;
font-weight: 600;
color: #ffffff;
`;


const Button1 = styled(Button)`
height: 60px;
border-left: 1px solid silver;
&:hover{
  border-bottom: 5px solid #4ec2e2;
  background-color: #ffffff;
}
border-radius: 0;

`
const Navbar1 = styled(Navbar)`
padding: 0px;
`
const Color = styled.div`
color: white;
`

const Button2 = styled(Button)`
height: 60px;
border-left: 1px solid silver;
&:hover{
  border-bottom: 5px solid #4ec2e2;
  background-color: #ffffff;
}
border-radius: 0;
border-right: 1px solid silver;
`

const Prof = styled.div`
width: 900px;
margin: 100px auto -80px auto;
color:#7387a9;
font-size: 14;
font-weight: 800;
`;
const Flex = styled.div`
display: flex;
align-items: center;
`;

const App4 = styled.div`
padding: 20px;
border: 1px solid #e5e9f4;
margin-top: 100px;
margin-left: auto;
margin-right: auto;
width: 900px;
background-color: #fff;
height: auto;
`;

const SaveButton = styled.div`
margin-top: 10px;
margin-left: auto;
margin-right: auto;
width: 900px;
text-align: right;
align-items: right;
height: auto;
`;

const Logo = styled.div`
margin-right: 20px;
  margin-top: 20px;
  margin-left: 50px;
`;

const Napis3 = styled.div`
margin-left: 20px;
font-size: 16px;
color: #acb7bb;
`;

const Napislogo = styled.div`
font-size: 16px;
color: #acb7bb;
align: center;
`;

const Napislogo2 = styled.div`
font-size: 20px;
  color: #f8f8f8;
`;

const Napis2 = styled.div`
margin-left: 15px;
font-size: 16px;
color: #4ec2e2;
`;

const Napis = styled.div`

font-size: 30px;
color: #bb99ff;
`;

const Custombutton1 = styled.div`
background-color: transparent;
`;

export default SignIn;
