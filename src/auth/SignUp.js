import React, { Component } from 'react';
import { Button } from 'reactstrap';
import AuthTemplate from './AuthTemplate';
import { Field, Form } from 'react-final-form';
import renderInput from '../components/form/Input';
import renderCheckbox from '../components/form/Checkbox';
import { sleep } from '../lib/utils';
import { composeValidators, required, email } from '../lib/validation';
import { FormFooter, StyledLink, Title } from '../components/styled/form';

class SignIn extends Component {
  onSubmit = async data => {
    await sleep(1000);
    console.log(data);
  };

  render() {
    return (
      <AuthTemplate>
        <Title>Create new account</Title>
        <Form
          onSubmit={this.onSubmit}
          render={({ handleSubmit, pristine, invalid }) => (
            <form onSubmit={handleSubmit}>
              <Field
                name="email"
                component={renderInput}
                type="text"
                label="Email address:"
                validate={composeValidators(required, email)}
              />
              <Field
                name="password"
                component={renderInput}
                type="password"
                label="Password:"
                validate={required}
              />
              <Field
                name="privacy"
                component={renderCheckbox}
                type="checkbox"
                label="I agree to the Terms of Service and Privacy Policy"
                validate={required}
              />
              <FormFooter>
                <Button size="lg" color="primary" block>
                  Sign Up
                </Button>
                <StyledLink to={'/signin'}>
                  Already have an account? Sign in
                </StyledLink>
              </FormFooter>
            </form>
          )}
        />
      </AuthTemplate>
    );
  }
}

export default SignIn;
