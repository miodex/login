import React, { Component } from 'react';
import { Button } from 'reactstrap';
import AuthTemplate from './AuthTemplate';
import { Field, Form } from 'react-final-form';
import renderInput from '../components/form/Input';
import renderCheckbox from '../components/form/Checkbox';
import { sleep } from '../lib/utils';
import { composeValidators, required, email } from '../lib/validation';
import { FormFooter, StyledLink, Title } from '../components/styled/form';

class SignIn extends Component {
  onSubmit = async data => {
    await sleep(1000);
    console.log(data);
  };

  render() {
    return (
      <AuthTemplate>
        <Title>Change your password</Title>
        <Form
          onSubmit={this.onSubmit}
          render={({ handleSubmit, pristine, invalid }) => (
            <form onSubmit={handleSubmit}>
              <Field
                id="pas1"
                name="password1"
                component={renderInput}
                type="password"
                label="Password:"
                validate={required}
              />
              <Field
                id="pas2"
                name="password2"
                component={renderInput}
                type="password"
                label="Repeat password:"
                validate={required}
              />
              <FormFooter>
                <Button size="lg" color="primary" block>
                  Change your password
                </Button>
                <StyledLink to={'/signin'}>
                  Back to sign in
                </StyledLink>
              </FormFooter>
            </form>
          )}
        />
      </AuthTemplate>
    );
  }
}

export default SignIn;
