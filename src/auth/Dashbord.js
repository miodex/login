import React, { Component } from 'react';
import styled from 'styled-components';
import {Col, Card, CardImg, FormGroup, Label, Input, FormText, Row, Container, Media,  Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavDropdown,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    Dropdown,
    DropdownItem, Table } from 'reactstrap';
import logo from '../logo.png';
import red from '../red.png';
import { Button } from "reactstrap";
import { Form, Field } from "react-final-form";
import NavbarGlowny from "./NavbarGlowny";
import AuthTemplate from "./AuthTemplate";
import renderInput from "../components/form/Input";
import renderCheckbox from "../components/form/Checkbox";
import { sleep } from "../lib/utils";
import { required, email, composeValidators } from "../lib/validation";
import { Link } from "react-router-dom";
import colors from "../colors";
import routes from "../routes";
import Avatar from 'react-avatar';
import UserAvatar from 'react-user-avatar';



class SignIn extends Component {
  onSubmit = async data => {
    await sleep(2000);
    console.log(data);
  };
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      
        <div>
          <NavbarGlowny>
            </NavbarGlowny>

        <Form
                  onSubmit={this.onSubmit}
                  render={({ handleSubmit, pristine, invalid }) => (
                    <form onSubmit={handleSubmit}>
          <div>
         
       
      </div>

                  <App4>
 
              <RowScore>
              <Colscore sm="3">
    Try the URL:
             </Colscore>
             <Col sm="6">
        <Field
                        name="password"
                        component={renderInput}
                        type="password"
                        label=""
                        validate={required}
                      />          
        </Col>
                    <Colscore sm="3">
         <Button color="primary">
  <Napislogo2>
  <b>Analyze</b>
  </Napislogo2>
 </Button> 
</Colscore>


       </RowScore>
      
   </App4>

   <App5>
     <RowScore>
       <Colscoreran sm="2">
       Score range:
       </Colscoreran>
     <Colscore sm="3">
      <Czerwone>   </Czerwone>
   Negative: (-1 &mdash; -0,75) 
   </Colscore>
   <Colscore sm="3">
    <Zolte> </Zolte>
   Neutral: (-0,75 &mdash; 0,25)  </Colscore>
   <Colscore sm="3"> 
   <Zielone> </Zielone>
   Positive:  (0,25 &mdash; 1,0)
   </Colscore>
   </RowScore>
           </App5>
           
       <App7>    
<App6>
  <RowScore>
<Napisscore>
<img src={logo}  alt="logo" />
&emsp; Your score:  &emsp;
    </Napisscore>
   <Napiswynik> 0,75 </Napiswynik> 
</RowScore>
    </App6>
    </App7>
        </form>
    
                  )}
                  /> 
                  </div>
    );
  }
}
const StyledLink = styled(Link).attrs({  })`
margin-left: 20px;
font-weight: 600;
color: #ffffff;
`;

const Napisscore = styled.div`
font-size: 20px;
color: #111111;
align: center;
font-weight: 600;
`;

const Napiswynik = styled.div`
font-size: 26px;
color: #00ff00;
align: center;
font-weight: 600;
`;

const RowScore = styled(Row)`
  width: 100%;
`;


const Colscore = styled(Col)`
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
`;

const Colscoreran = styled(Col)`
  padding: 0;
  margin-left: 30px;
  display: flex;
  align-items: center;
`;


const Zielone = styled.div`
border-radius: 50%;
width: 14px;
height: 14px;
margin-right: 5px;
  background-color: #00ff00;
`;

const Zolte = styled.div`
border-radius: 50%;
width: 14px;
height: 14px;
margin-right: 5px;
  background-color: #ffff00;
`;

const Czerwone = styled.div`
border-radius: 50%;
width: 14px;
height: 14px;
margin-right: 5px;
  background-color: #ff0000;
`;




const StyledLinkNav = styled(Link).attrs({  })`
font-weight: 100;
color: #000000;
`;

const Button1 = styled(Button)`
height: 60px;
border-left: 1px solid silver;
&:hover{
  border-bottom: 5px solid #4ec2e2;
  background-color: #ffffff;
}
border-radius: 0;

`
const Navbar1 = styled(Navbar)`
padding: 0px;
`
const Color = styled.div`
color: white;
`

const Button2 = styled(Button)`
height: 60px;
border-left: 1px solid silver;
&:hover{
  border-bottom: 5px solid #4ec2e2;
  background-color: #ffffff;
}
border-radius: 0;
border-right: 1px solid silver;
`


const Prof = styled.div`
width: 680px;
margin: 100px auto -80px auto;
color:#7387a9;
font-size: 14;
font-weight: 800;
`;

const Flex = styled.div`
display: flex;
align-items: center;
`;

const App6 = styled.div`
padding: 20px;
border: 1px solid #e5e9f4;
width: 300px;
background-color: #fff;
height: auto;
`;


const App7 = styled.div`
display: flex;
margin-top: 10px;
margin-left: auto;
margin-right: auto;
max-width: 900px;
height: auto;
justify-content: flex-end;
`;

const App5 = styled.div`
padding: 20px;
border: 1px solid #e5e9f4;
margin-top: 1px;
margin-left: auto;
margin-right: auto;
max-width: 900px;
background-color: #fff;
height: auto;

`;

const App4 = styled.div`
padding: 20px;
border: 1px solid #e5e9f4;
margin-top: 100px;
margin-left: auto;
margin-right: auto;
align: center;
max-width: 900px;
background-color: #fff;
height: auto;

    align-items: center;
`;

const Logo = styled.div`
margin-right: 20px;
  margin-top: 20px;
  margin-left: 50px;
`;

const Napis3 = styled.div`
margin-left: 20px;
font-size: 16px;
color: #acb7bb;
`;

const Napislogo = styled.div`
font-size: 16px;
color: #acb7bb;
align: center;
`;

const Napislogo2 = styled.div`
font-size: 20px;
  color: #f8f8f8;
`;

const Napis2 = styled.div`
margin-left: 15px;
font-size: 16px;
color: #4ec2e2;
`;

const Napis = styled.div`

font-size: 30px;
color: #bb99ff;
`;
export default SignIn;
