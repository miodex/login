import styled from 'styled-components';
import React from 'react';

const AuthTemplate = ({ children }) => {
  return (
    <Wrapper>
      <View>
        <Header>
          <Logo src="./img/logo.png" />
          <AppName>AJP Sentiment Analysis</AppName>
        </Header>
        {children}
      </View>
    </Wrapper>
  );
};

const View = styled.div`
  background-color: #feffff;
  border: 1px solid #ebf2fa;
  border-radius: 3px;
  box-shadow: 0px 2px 10px 0px rgba(233, 233, 233, 0.5);
  padding: 40px;
  margin: 120px auto;
  width: 500px;
`;

const Logo = styled.img`
  width: 30px;
  height: 30px;
`;

const AppName = styled.div`
  margin-left: 15px;
  font-size: 15px;
  color: #7387a9;
  font-weight: bold;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const Header = styled.div`
  margin-bottom: 2rem;
  display: flex;
  align-items: center;
`;

export default AuthTemplate;
