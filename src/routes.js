export default {
    signIn: "/signin",
    signUp: "/signup",
    resetPassword: "/reset-password",
    setPassword: "/set-password/:token",
    profile: "/profile",
    dashboard: "/dashboard",
  };
  
