import React from 'react';

import {FormFeedback, FormGroup, Input, Label} from 'reactstrap';

const renderInput = ({ input, meta, label, ...props }) => {
  const showError = meta.touched && meta.error;
  return (
    <FormGroup>
      <Label for={input.field}>{label}</Label>
      <Input {...input} {...props} invalid={showError ? true : false}/>
      {showError && (
        <FormFeedback invalid={showError ? 'true' : 'false'}>{meta.error}</FormFeedback>
      )}
    </FormGroup>
  );
};

export default renderInput;
