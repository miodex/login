import React from 'react';

import { FormFeedback, FormGroup, Input, Label } from 'reactstrap';

const renderCheckbox = ({ input, meta, label, ...props }) => {
  const showError = meta.touched && meta.error;
  return (
    <FormGroup className="custom-control custom-checkbox">
      <Input
        {...input}
        {...props}
        invalid={showError ? true : false}
        className="custom-control-input"
        id={input.name}
      />
      <Label for={input.name} className="custom-control-label">
        {label}
      </Label>
      {showError && (
        <FormFeedback invalid={showError ? 'true' : 'false'}>
          {meta.error}
        </FormFeedback>
      )}
    </FormGroup>
  );
};

export default renderCheckbox;
