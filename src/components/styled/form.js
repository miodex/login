import {Link} from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link).attrs({ className: "btn btn-block btn-lg btn-link" })`
  margin-top: 1rem !important;
`;

const Title = styled.h1`
  font-size: 2rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
  color: #9585c4;
  font-weight: lighter;
`;

const FormFooter = styled.div`
  margin-top: 2rem;
  align-items: center;
`;

export {StyledLink, FormFooter, Title};