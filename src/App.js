import React, { Component } from 'react';
import './Bootstrap.css';

import SignIn from './auth/SignIn';
import SignUp from './auth/SignUp';
import ResetPassword from './auth/ResetPassword';
import ChangeYourPassword from './auth/ChangeYourPassword';
import Profile from './auth/Profile';
import Dashbord from './auth/Dashbord';
import Contact from './auth/Contact';

import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path={'/'} render={() => <Redirect to={'/signin'} />} />

          <Route path={'/signin'} component={SignIn} />
          <Route path={'/signup'} component={SignUp} />
          <Route path={'/resetpassword'} component={ResetPassword} />
          <Route path={'/changeyourpassword'} component={ChangeYourPassword} />
          <Route path={'/profile'} component={Profile} />
          <Route path={'/dashbord'} component={Dashbord} />
          <Route path={'/contact'} component={Contact} />
        </Switch>
      </Router>
    );
  }
}

export default App;
